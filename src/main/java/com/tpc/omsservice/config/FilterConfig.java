package com.tpc.omsservice.config;

import com.tpc.omsservice.filter.ReplaceStreamFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    @Autowired
    private ReplaceStreamFilter replaceStreamFilter;
    /**
     * 注册过滤器
     *
     * @return FilterRegistrationBean
     */
    @Bean
    public FilterRegistrationBean someFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(replaceStreamFilter);
        registration.addUrlPatterns("/*");
        registration.setName("streamFilter");
        return registration;
    }

}
