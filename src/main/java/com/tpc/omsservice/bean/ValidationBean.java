package com.tpc.omsservice.bean;

import lombok.Data;

@Data
public class ValidationBean {
    private String appKey;
    private String appSecret;
    private String appType;
}
