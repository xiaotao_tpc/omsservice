package com.tpc.omsservice.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.UUID;

public class JSONUtil {
    public static String resultSetToJson(ResultSet rs) throws SQLException, JSONException {
        // json数组
        JSONArray array = new JSONArray();

        // 获取列数
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        String uuid = UUID.randomUUID().toString();
        // 遍历ResultSet中的每条数据
        while (rs.next()) {
            JSONObject jsonObj = new JSONObject();
            // 遍历每一列
            for (int i = 1; i <= columnCount; i++) {
                String columnName =metaData.getColumnLabel(i).toLowerCase();
                String value = "";
                if(rs.getString(columnName)!=null) {
                    value = rs.getString(columnName).trim();
                }else{
                    value="";
                }
                if(value.indexOf("[")>-1 || value.indexOf("]") >-1 || value.indexOf("{") >-1 || value.indexOf("}") >-1 ){
                    value =value.replaceAll("\\[","【");
                    value =value.replaceAll("]","】");
                    value =value.replaceAll("\\{","<");
                    value = value.replaceAll("}",">");
                }
                jsonObj.put(columnName, value);
            }
            jsonObj.put("gzid",uuid);
            array.add(jsonObj);
        }
        return array.toString();
    }
}
