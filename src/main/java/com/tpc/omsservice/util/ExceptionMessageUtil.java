package com.tpc.omsservice.util;

public class ExceptionMessageUtil {
    public static String getMessage(String content){
        String message = "";
        if(content!=null && content.indexOf("。")>-1){
            String[] split = content.split("。");
            if(split.length>0){
                message = split[0];
            }else{
                message = content;
            }
        }else{
            message = content;
        }
        return message;
    }
}
