package com.tpc.omsservice.util;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * JDBC数据库操作工具类  简化数据库操作
 * @author wxisme
 *
 */
@Slf4j
public class JDBCUtil {

    private static DruidDataSource dataSource = null;

    static {
        try {
            File directory = new File(".");
            String path = directory.getCanonicalPath();
            log.info("当前目录：{}",path);
            path = path + "/db.properties";
            Properties props = new Properties();
            FileInputStream in = new FileInputStream(path);
            props.load(in);
            String dbDriver = props.getProperty("dbDriver");
            String dbURL = props.getProperty("dbURL");
            String dbUser = props.getProperty("dbUser");
            String dbPwd = props.getProperty("dbPwd");
            log.info("{}---{}---{}--{}",dbDriver,dbURL,dbUser,dbPwd);
            dataSource = new DruidDataSource();
            dataSource.setDriverClassName(dbDriver);
            dataSource.setUrl(dbURL);
            dataSource.setUsername(dbUser);
            dataSource.setPassword(dbPwd);
            dataSource.setMinIdle(5);
            dataSource.setMaxActive(2000);
            dataSource.setRemoveAbandoned(true);
            dataSource.setRemoveAbandonedTimeout(60);
            dataSource.setMaxWait(10000);
            dataSource.setInitialSize(1);
//            dataSource.setTestOnBorrow(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    public static Connection getConnection()  {


        Connection connection = null;
        connection = dataSource.getConnection();
        return connection;
    }


    /**
     * 关闭io资源
     * @param io
     */
    public static void closeFile(Closeable ... io) {
        for(Closeable temp : io) {
            if(temp != null) {
                try {
                    temp.close();
                } catch (IOException e) {
                    System.out.println("文件关闭失败");
                    e.printStackTrace();
                }
            }
        }
    }

    //关闭JDBC资源  注意顺序
    public static void close(ResultSet rs,Statement ps,Connection conn) {
        try {
            if(rs!=null){
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if(ps!=null){
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if(conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void close(Statement ps,Connection conn){
        try {
            if(ps!=null){
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if(conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void close(Connection conn){
        try {
            if(conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void close(Connection conn,PreparedStatement ps){
        try {

            if(ps!=null){
                ps.close();
            }

            if(conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void close(CallableStatement call){
        try {
            if(call!=null){
                call.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //获取当前表的列
    public static String getTableColumn(String tableName)  {
        Connection connection = null;
        DatabaseMetaData metadata = null;
        List<String> listColumn  = new ArrayList<>();
        String nameColumn = "";
        try {
            connection = JDBCUtil.getConnection();
            metadata = connection.getMetaData();
            ResultSet resultSet =
                    metadata.getColumns(null, null, tableName.toUpperCase(), null);
            while (resultSet.next()) {
                String name = resultSet.getString("COLUMN_NAME");
//                nameColumn = nameColumn + name.toLowerCase() + ",";
                listColumn.add(name.toLowerCase());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(connection);
        }
        nameColumn = listColumn.stream().distinct()
                        .map(String::valueOf).collect(Collectors.joining(","))+",";
        return nameColumn;
    }

    //获取列及列类型
    public static Map<String,String> getTableColumnType(String tableName) throws SQLException {
        Connection connection = JDBCUtil.getConnection();
        DatabaseMetaData metadata = null;
        Map<String,String> map = new HashMap<>();
        try {
            metadata = connection.getMetaData();
            ResultSet resultSet =
                    metadata.getColumns(null, null, tableName.toUpperCase(), null);
            while (resultSet.next()) {
                String name = resultSet.getString("COLUMN_NAME").toLowerCase();
                String nameType = resultSet.getString("TYPE_NAME");
                map.put(name,nameType);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JDBCUtil.close(connection);
        }
        return map;
    }

}