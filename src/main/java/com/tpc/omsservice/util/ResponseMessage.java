package com.tpc.omsservice.util;

public class ResponseMessage<T> {

    private static final String CODE_SUCCESS = "200";

    private static final String CODE_FAIL = "400";

    private static final String MSG_SUCCESS="成功!";

    private static final String MSG_FAIL="失败";

    public ResponseMessage(){
    }
    public ResponseMessage(String code ){
        this.code=code;
    }
    public ResponseMessage(String code,T data ){
        this.code=code;
        this.data=data;
    }
    public ResponseMessage(String code, String msg){
        this.code = code;
        this.msg = msg;
    }
    public ResponseMessage(String code, String msg,T data) {
        this.code = code;
        this.msg = msg;
        this.data=data;
    }
    public static ResponseMessage success(){
        return new ResponseMessage(CODE_SUCCESS,MSG_SUCCESS);
    }

    public static ResponseMessage success(Object data){
        return new ResponseMessage(CODE_SUCCESS,MSG_SUCCESS, data);
    }

    public static ResponseMessage fail(){
        return new ResponseMessage(CODE_FAIL, MSG_FAIL);
    }

    public static ResponseMessage fail(Object data) {
        return new ResponseMessage(CODE_FAIL,MSG_FAIL, data);
    }

    public static ResponseMessage fail500(Object data){
        return new ResponseMessage("500", MSG_FAIL,data);
    }

    private String code;

    private String msg;

    public T data;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public T getData() {
        return data;
    }

    public void setData(T entity) {
        this.data = data;
    }
}
