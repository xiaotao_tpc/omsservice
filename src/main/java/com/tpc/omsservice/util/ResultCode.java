package com.tpc.omsservice.util;




public enum ResultCode {

    SUCCESS(200,"插入成功！"),
    FAILED(222,"插入失败！"),
    VALIDATE_FAILED(400, "参数校验失败"),
    ERROR(500,"未知错误");
    private int code;
    private String msg;

    ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
