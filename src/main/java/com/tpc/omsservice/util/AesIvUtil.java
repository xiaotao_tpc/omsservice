package com.tpc.omsservice.util;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.net.NetUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

@Slf4j
// https://tool.lmeee.com/jiami/aes 测试一致
public class AesIvUtil {
    //加密
    public static String encode(String stext, String skey, String siv) throws Exception {
        if (skey == null) {
            return null;
        }
        if (skey.length() != 16) {
            return null;
        }
        byte[] keyByte = skey.getBytes("utf-8");
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");//算法/模式/补码方式
        byte[] ivByte = siv.getBytes("utf-8");
        IvParameterSpec iv = new IvParameterSpec(ivByte);   //使用CBC模式，需要一个向量iv，可增加加密算法的强度
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);
        byte[] encrype = cipher.doFinal(stext.getBytes());
        return Base64.getEncoder().encodeToString(encrype);      //此处使用BASE64做转码功能，同时能起到2次加密的作用。
    }

    //解密
    public static String decode(String stext, String skey, String siv) throws Exception {
        try {
            if (skey == null) {
                return null;
            }
            if (skey.length() != 16) {
                return null;
            }
            byte[] keyByte = skey.getBytes("utf-8");
            SecretKeySpec keySpec = new SecretKeySpec(keyByte, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");//算法/模式/补码方式
            IvParameterSpec iv = new IvParameterSpec(siv.getBytes());   //使用CBC模式，需要一个向量iv，可增加加密算法的强度
            cipher.init(Cipher.DECRYPT_MODE, keySpec, iv);
            byte[] encrype = Base64.getDecoder().decode(stext);

            byte[] origin = cipher.doFinal(encrype);
            String str = new String(origin);
            return str;

        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) throws Exception {
        String key = "jiNanskA@99#946F";
        String iv = "jiNanskA@99#946F";
        String entid = "E44CKHJD57C";
        String dates = "2099-12-31";
        InetAddress inetAddress = InetAddress.getLocalHost();
        log.info("{}",NetUtil.getMacAddress(inetAddress));
        //NetUtil.getMacAddress(InetAddress.getLocalHost())
        String mac = NetUtil.getMacAddress(inetAddress).replace("-", "");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("entid", entid);
        jsonObject.put("dates", dates);
        jsonObject.put("mac", mac);

        String content = jsonObject.toJSONString();

        String enStr = encode(content, key, iv);
        System.out.println("加密的字符串：" + enStr);
        String deStr = decode(enStr, key, iv);
        System.out.println("解密的字符串：" + deStr);

        String test = "KjK923Qr1pXO6ZftzQTP59ZsyBiryL9p/203efmAon09fZeeHMMSZDbLxys29mgK6h8JxL/oomDdnjxrB4HTO4hjwudHYVMx8DLU2JRzp5A=";
        deStr = decode(test, key, iv);
        System.out.println("解密的字符串：" + deStr);
        JSONObject jsonObject1 = JSON.parseObject(deStr);

        long dates1 = DateUtil.between(DateUtil.parse(jsonObject1.getString("dates")), DateUtil.date(), DateUnit.DAY);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dates4 = format.parse(jsonObject1.getString("dates"));
        Date newDates = format.parse(format.format(new Date()));
        long i = (newDates.getTime() - dates4.getTime() )/(24*60*60*1000);

        log.info("{},{}",dates1,i);

    }


}
