package com.tpc.omsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableCaching
@SpringBootApplication
public class OmsserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OmsserviceApplication.class, args);
    }

}
