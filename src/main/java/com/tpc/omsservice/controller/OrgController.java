package com.tpc.omsservice.controller;


import com.alibaba.fastjson.JSONObject;
import com.tpc.omsservice.bean.ValidationBean;
import com.tpc.omsservice.service.ValidationService;
import com.tpc.omsservice.util.ResponseMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@ResponseBody
@RequestMapping("/jnsk")
@Slf4j
public class OrgController {

    @Autowired
    private ValidationService validationService;

    @PostMapping("/appSecret")
    public ResponseMessage updateAppSecret(@RequestBody ValidationBean validationBean){
        try {
            log.info(validationBean.toString());
            ValidationBean validation = validationService.updateAppSecret(validationBean);
            if(validation.getAppSecret().equals("1")){
                return ResponseMessage.fail(validation);
            }
            log.info("AAA{}",JSONObject.toJSONString(ResponseMessage.success(validation)) );
            return ResponseMessage.success(validation);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ResponseMessage.fail(throwables.getMessage());
        }
    }
}
