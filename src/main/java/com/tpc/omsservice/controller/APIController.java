package com.tpc.omsservice.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tpc.omsservice.service.ManageService;
import com.tpc.omsservice.util.*;

import com.tpc.omsservice.wrapper.RequestWrapper;
import lombok.extern.slf4j.Slf4j;
//import org.apache.http.Consts;
//import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RestController
@ResponseBody
@RequestMapping("/jnsk")
@Slf4j
public class APIController {

    @Autowired
    private ManageService manageService;

    @RequestMapping("/oms")
    public ResponseMessage apiController(@RequestBody JSONObject jsonObject) throws UnsupportedEncodingException {
        JSONObject afterJSON = TransformJSONUtil.transToLowerObject(jsonObject.toJSONString());
        log.info("接收请求参数:"+afterJSON.toJSONString());
        String type = afterJSON.getString("type");
        ResponseMessage backJSON = new ResponseMessage();
        if (type != null && type.length() > 0 && jsonObject.getString("type").equals("Request")) {
            backJSON = manageService.RequestService(afterJSON);
        } else if (type != null && type.length() > 0 && jsonObject.getString("type").equals("Response")) {
            backJSON = manageService.ResponseService(afterJSON);
        } else if (type != null && type.length() > 0 && jsonObject.getString("type").equals("Back")) {
            backJSON = manageService.BackService(afterJSON);
        } else {
            String message = "没有符合当前请求的类型！";
            backJSON = ResponseMessage.fail(message);
        }
        log.info("返回JSON:" + JSONObject.toJSONString(backJSON));
        return backJSON;
    }
}
