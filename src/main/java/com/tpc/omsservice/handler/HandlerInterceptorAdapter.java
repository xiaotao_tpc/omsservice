package com.tpc.omsservice.handler;

import cn.hutool.core.net.NetUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tpc.omsservice.bean.ValidationBean;
import com.tpc.omsservice.service.ValidationService;
import com.tpc.omsservice.util.AesIvUtil;
import com.tpc.omsservice.util.JDBCUtil;
import com.tpc.omsservice.util.ResponseMessage;
import com.tpc.omsservice.wrapper.RequestWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

@Slf4j
@Component
public class HandlerInterceptorAdapter implements HandlerInterceptor {


    @Autowired
    private ValidationService validationService;

    /**
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String key = "jiNanskA@99#946F";
        String iv = "jiNanskA@99#946F";
        RequestWrapper requestWrapper = new RequestWrapper(request);
        log.info("接收请求JSON:"+requestWrapper.getBodyString());
        ValidationBean validationBean = JSON.parseObject(requestWrapper.getBodyString(), ValidationBean.class);
        String appKey = validationBean.getAppKey();
        String appSecret = validationBean.getAppSecret();

        if(validationBean.getAppType()==null){
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json; charset=utf-8");
            PrintWriter out = null;
            out = response.getWriter();
            out.append(JSON.toJSONString(ResponseMessage.fail("AppKey对应的类型未维护，请检查！")));
            return false;
        }else {
            String appType = validationBean.getAppType();
            try{
                File directory = new File(".");
                String path = directory.getCanonicalPath();
                log.info("当前目录：{}",path);
                path = path + "/db.properties";
                Properties props = new Properties();
                FileInputStream in = new FileInputStream(path);
                props.load(in);
                String STR = props.getProperty("STR");
                String decode = AesIvUtil.decode(STR, key, iv);
                JSONObject jsonObject = JSON.parseObject(decode);
                String macAddress = NetUtil.getMacAddress(InetAddress.getLocalHost());
                if(!jsonObject.getString("entid").equals(appKey) || !jsonObject.getString("mac").equals(NetUtil.getMacAddress(InetAddress.getLocalHost())) ){
                    response.setCharacterEncoding("UTF-8");
                    response.setContentType("application/json; charset=utf-8");
                    PrintWriter out = null;
                    out = response.getWriter();
                    out.append(JSON.toJSONString(ResponseMessage.fail("当前企业未授权，请检查！")));
                    return false;
                }
                if(jsonObject.getString("dates")!=null){
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date dates4 = format.parse(jsonObject.getString("dates"));
                    Date newDates = format.parse(format.format(new Date()));
                    long i = (newDates.getTime() - dates4.getTime() )/(24*60*60*1000);
                    if(i>0){
                        response.setCharacterEncoding("UTF-8");
                        response.setContentType("application/json; charset=utf-8");
                        PrintWriter out = null;
                        out = response.getWriter();
                        out.append(JSON.toJSONString(ResponseMessage.fail("当前企业授权已过期，请检查！")));
                        return false;
                    }
                }
            }catch (Exception e){
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/json; charset=utf-8");
                PrintWriter out = null;
                out = response.getWriter();
                out.append(JSON.toJSONString(ResponseMessage.fail("当前未维护授权码，请检查！")));
                return false;
            }





            ValidationBean validation = validationService.getAppSecret(appKey, appType);
            log.info("validation:{}",validation);
            if (validation != null && validation.getAppSecret().equals(appSecret)) {
                return true;
            } else {
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/json; charset=utf-8");
                PrintWriter out = null;
                out = response.getWriter();
                out.append(JSON.toJSONString(ResponseMessage.fail("AppKey与AppSecret不匹配，请检查！")));
                return false;
            }


        }
    }

    /**
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    /**
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
