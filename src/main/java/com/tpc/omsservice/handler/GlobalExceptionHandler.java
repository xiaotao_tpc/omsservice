package com.tpc.omsservice.handler;

import com.tpc.omsservice.util.ExceptionMessageUtil;
import com.tpc.omsservice.util.ResponseMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler({Exception.class})
    public ResponseMessage exceptionHandler(HttpServletRequest request,Exception e){
        log.info("错误信息："+e.getMessage());
        return ResponseMessage.fail500(ExceptionMessageUtil.getMessage(e.getMessage()));
    }
}
