package com.tpc.omsservice.service;

import com.tpc.omsservice.bean.ValidationBean;
import com.tpc.omsservice.util.JDBCUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;


@Slf4j
@Service
public class ValidationService {

//    @Cacheable(value = {"Validation"},key = "#appKey")
    public ValidationBean getAppSecret(String appKey,String appType) throws Exception{
        Connection connection = null ;
        connection = JDBCUtil.getConnection();
        log.info("+++++++++++++++++++++++++++++++++");
        QueryRunner queryRunner = new QueryRunner();
        String sql = "";
        if(appType.equals("ORG")) {
            sql = "select orgid as appKey,appSecret from OrgDoc where OrgId= ? ";
        }else if(appType.equals("ENT")){
            sql = "select entid as appKey,appSecret from EntDoc where EntId = ?";
        }
        log.info("sql:{}",sql);
        ValidationBean validation = queryRunner.query(connection, sql, new BeanHandler<>(ValidationBean.class), appKey);
        JDBCUtil.close(connection);
        return validation;
    }

    @CacheEvict(value = {"Validation"},allEntries = true)
    public ValidationBean updateAppSecret(ValidationBean validationBean) throws SQLException {
        Connection connection = null;
        connection = JDBCUtil.getConnection();
        log.info("---------------------------{}",connection);
        QueryRunner queryRunner = new QueryRunner();
        String sql = "update orgDoc set appSecret = ? where orgId = ? ";
        int update = queryRunner.update(connection, sql,  validationBean.getAppSecret() , validationBean.getAppKey() );
        if(update>0){
            return validationBean;
        }else{
            validationBean.setAppSecret("1");
            return validationBean;
        }
    }

    public static void main(String[] args) {
        ConcurrentMapCacheManager concurrentMapCacheManager = new ConcurrentMapCacheManager();
        Cache cache = concurrentMapCacheManager.getCache("Validation");
        log.info(String.valueOf(cache.get("")));
    }
}
