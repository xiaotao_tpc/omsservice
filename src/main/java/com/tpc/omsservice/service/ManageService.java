package com.tpc.omsservice.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tpc.omsservice.util.JDBCUtil;
import com.tpc.omsservice.util.JSONUtil;
import com.tpc.omsservice.util.ResponseMessage;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;
import org.apache.commons.dbutils.QueryRunner;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
@Service
@Slf4j
public class ManageService {

    public ResponseMessage RequestService(JSONObject jsonObject) {
        Connection conn = null;
        JSONArray jsonArray = jsonObject.getJSONArray("date");
        String gzid = "";
        String orgid = "";
        String biaoshi = "";
        String message = "";
        int q=0;
        if(jsonArray!=null && jsonArray.size()>0) {
            gzid=jsonObject.getString("gzid");
            orgid = jsonObject.getString("appkey");
            biaoshi = jsonObject.getString("tablename");
            if(gzid != null && biaoshi !=null && gzid.length()>0 && biaoshi.length()>0) {
                Map<String,String> map = null;
                try {
                    String info = JDBCUtil.getTableColumn(biaoshi);
                    log.info("====={}",info);
                    if(info.length()>0) {
                        if(info.indexOf("gzid")>-1) {
                            map = JDBCUtil.getTableColumnType(biaoshi);
                            String[] stringArray = info.split(",");
                            Object[][] objects = new Object[jsonArray.size()][stringArray.length];
                            String sql = "insert into " + biaoshi + " (" + info.substring(0, info.length() - 1) + ") values ( ";
                            for (int i = 0; i < stringArray.length; i++) {
                                sql = sql + "?,";
                            }
                            sql = sql.substring(0, sql.length() - 1) + ")";
                            log.debug("插入语句：" + sql);
                            conn = JDBCUtil.getConnection();
                            conn.setAutoCommit(false);
                            QueryRunner queryRunner = new QueryRunner();
                            for (int j = 0; j < jsonArray.size(); j++) {
                                JSONObject jobj = jsonArray.getJSONObject(j);
                                jobj.put("gzid", gzid);
                                if(jobj.getString("orgid")==null) {
                                    jobj.put("orgid",orgid);
                                }
                                if(jobj.getString("ownerid")==null){
                                    jobj.put("ownerid",orgid);
                                }
                                String columns = "select ";
                                for (int i = 0; i < stringArray.length; i++) {
                                    Object obj = null;
                                    if (map.get(stringArray[i]).equals("int")) {
                                        columns = columns + (jobj.getString(stringArray[i]) == null ? 0 : jobj.getString(stringArray[i])) + " as " + stringArray[i] + ",";
                                        int columnsValue = 0;
                                        if (jobj.getBigDecimal(stringArray[i]) != null) {
                                            columnsValue = jobj.getIntValue(stringArray[i]);
                                        }
                                        obj = columnsValue;
                                    } else if (map.get(stringArray[i]).equals("decimal")) {
                                        columns = columns + (jobj.getString(stringArray[i]) == null ? 0 : jobj.getString(stringArray[i])) + " as " + stringArray[i] + ",";
                                        BigDecimal bigDecimal = new BigDecimal(0);
                                        if (jobj.getBigDecimal(stringArray[i]) != null) {
                                            bigDecimal = jobj.getBigDecimal(stringArray[i]);
                                        }
                                        obj = bigDecimal;
                                    } else {
                                        columns = columns + "'" + (jobj.getString(stringArray[i]) == null ? 0 : jobj.getString(stringArray[i])) + "' as " + stringArray[i] + ",";
                                        obj = jobj.getString(stringArray[i]) == null ? "" : jobj.getString(stringArray[i]);

                                    }
                                    objects[j][i] = obj;
                                }
                                log.debug("插入列：" + columns.substring(0, columns.length() - 1));
                            }
                            queryRunner.batch(conn, sql, objects);
                            String procSql = "{call sp_jlOmsExec(?,?,?,?)}";
                            String deleteSql = "delete from " + biaoshi + " where ltrim(rtrim(gzid)) =" + "'" + gzid + "'";
                            log.debug(procSql + "["+gzid+","+biaoshi+",0,0]");
//                            log.debug(deleteSql);
                            CallableStatement callableStatementProc = conn.prepareCall(procSql);
                            callableStatementProc.setString(1, gzid);
                            callableStatementProc.setString(2, biaoshi);
                            callableStatementProc.setInt(3, 0);
                            callableStatementProc.setInt(4, 0);
                            callableStatementProc.execute();
                            queryRunner.update(conn, deleteSql);
                            conn.commit();
                        }else{
                            message = biaoshi + "gzid字段不存在！ ";
                        }
                    }else{
                        message = biaoshi + "表和视图不存在！ ";
                    }
                } catch (Exception e) {
                    message = message + "错误 " +  e.getMessage();
                    try {
                        conn.rollback(); //发生异常，回滚
                    } catch (SQLException e1) {
                        message = message + "错误 " +  e.getMessage();
                    }
                }finally {
                    JDBCUtil.close(conn);
                }
            }else{
                message = message + "JSON格式不对，没有配置gzid或biaoshi";
            }
        }
        if(message.length()>0){
            return ResponseMessage.fail(message);
        }else {
            return ResponseMessage.success();
        }
    }

    public ResponseMessage ResponseService(JSONObject jsonObject)  {
        String message = "";
        JSONObject jsonData = jsonObject.getJSONObject("date");
        String action = "";
        if(jsonData!=null){
            action = jsonData.getString("action");
        }else{
            action = "fbWarehouseOrder";
        }

        String orderType = jsonObject.getString("tablename");
        String biaoshi = jsonObject.getString("backtablename");
        String orgid = jsonObject.getString("appkey");
        Connection conn = null;
        CallableStatement call = null;
        ResultSet rs = null;
        try {
            conn = JDBCUtil.getConnection();

            String procSql = " { call sp_jlOmsQuery(?,?,?,?,?,?,?,?) }";
//            log.debug(procSql);
//            log.debug("执行参数：'"+action+"','"+orderType+"','"+biaoshi+"','','"+orgid+"','',''");
//            List<Object[]> list = queryRunner.query(conn, procSql, new ArrayListHandler(), action, orderType, biaoshi, "", orgid, "", "");

//            if(list!=null){
//                jsonArray.add(list);
//            }
            call = conn.prepareCall(procSql);
            log.debug(procSql);
            log.debug("执行参数：'"+action+"','"+orderType+"','"+biaoshi+"','','"+orgid+"','',''");
            call.setString(1, action);
            call.setString(2, orderType);
            call.setString(3, biaoshi);
            call.setString(4, "");
            call.setString(5, orgid);
            call.setString(6, "");
            call.setString(7, "");
            call.registerOutParameter(8, OracleTypes.CURSOR);
            call.execute();
//            log.info("result:{}",result);
//            if(result){
                rs = (ResultSet) call.getObject(8);
//            }

            boolean flag = (rs==null);
            if(!flag){
                QueryRunner queryRunner = new QueryRunner();
                String s = JSONUtil.resultSetToJson(rs);
                JSONArray jsonArray = JSONArray.parseArray(s);
                if(jsonArray.size()==0){
                    message = message + "未查询到结果数据";
                }else {
                    Object[][] objects = new Object[jsonArray.size()][6];
                    String sql = "insert into tmp_callBack(orgid,ownerid,id,orderType,gzid,biaoshi) values (?,?,?,?,?,?)";
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JSONObject backJSON = jsonArray.getJSONObject(i);
                        String id = backJSON.getString("id");
                        String gzid = backJSON.getString("gzid");
                        objects[i][0] = orgid;
                        objects[i][1] = orgid;
                        objects[i][2] = id;
                        objects[i][3] = orderType;
                        objects[i][4] = gzid;
                        objects[i][5] = biaoshi;
                        log.debug("tmp_callBack插入语句" + sql);
                        log.debug("tmp_callBack插入语句插入列：'" + orgid + "','" + orgid + "'," + id + ",'" + orderType + "','" + gzid + "','" + biaoshi + "'");

                    }

                queryRunner.batch(conn,sql,objects);
                return ResponseMessage.success(jsonArray);
                }
            }else{
                message = message + "未查询到结果数据";
            }

//            ResultSet rs = null;
//            try {
//                conn = JDBCUtil.oracleConn();
//                call = conn.prepareCall(procSql);
//                log.debug(procSql);
//                log.debug("执行参数：'"+action+"','"+orderType+"','"+biaoshi+"','','"+orgid+"','',''");
//                call.setString(1, action);
//                call.setString(2, orderType);
//                call.setString(3, biaoshi);
//                call.setString(4, "");
//                call.setString(5, orgid);
//                call.setString(6, "");
//                call.setString(7, "");
//                call.registerOutParameter(8,oracle.jdbc.OracleTypes.CURSOR);
//                call.execute();
//                rs = call.getResultSet();
//                boolean flag = (rs==null);
//                if(!flag){
//                    resultJson = JSONUtil.resultSetToJson(rs);
//                }
//            } catch (SQLException e) {
////            if(message.indexOf(e.getMessage())<0) {
//                message = message + e.getMessage();
//                log.error(message);
////            }
//            } finally {
//                JDBCUtil.close(call);
//                JDBCUtil.close(conn);
//            }


//            if(list.size()>0){
//
//            }else{
//                message = message + "未查询到结果数据";
//            }
        } catch (SQLException e) {
            e.printStackTrace();
//            log.info("错误：{}",)
            message = message + e.getMessage();
            e.printStackTrace();
        }finally {
            JDBCUtil.close(rs,call,conn);
        }
        return  ResponseMessage.fail(message);
    }

    public ResponseMessage BackService(JSONObject jsonObject) {
        String message = "";
        String gzid = jsonObject.getString("gzid");
        String biaoshi = jsonObject.getString("biaoshi");
        Connection connection = null;
        CallableStatement callableStatementProc = null;
        String backProcSql = "{call sp_jlOmsExec(?,?,?,?)}";
        String backSql = "delete from tmp_callBack where gzid = ? ";

        try {
            connection = JDBCUtil.getConnection();
            log.debug("tmp_callBack过程回写语句" + backProcSql);
            log.debug("tmp_callBack过程回写语句参数：'"+gzid+"','tmp_callBack',0,0");
            log.debug("tmp_callBack删除语句" + backProcSql);
            log.debug("tmp_callBack删除语句参数：'"+gzid+"'");
            callableStatementProc = connection.prepareCall(backProcSql);
            callableStatementProc.setString(1, gzid);
            callableStatementProc.setString(2, "tmp_callBack");
            callableStatementProc.setInt(3, 0);
            callableStatementProc.setInt(4, 0);
            callableStatementProc.execute();
            QueryRunner queryRunner = new QueryRunner();
            queryRunner.update(connection,backSql,gzid);

        } catch (SQLException e) {
            e.printStackTrace();
            message=message+e.getMessage();
        }finally{
            JDBCUtil.close(connection);
        }
        if(message.length()>0){
            return  ResponseMessage.fail(message);
        }else{
            return ResponseMessage.success();
        }
    }

}
